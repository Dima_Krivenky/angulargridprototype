import {Component, Input, Output, EventEmitter, ChangeDetectionStrategy, ViewChild} from '@angular/core';
import {ItemsService, Item, AppStore} from './items';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import {AgGridNg2} from 'ag-grid-ng2/main';
import {GridOptions} from 'ag-grid/main';
import { MODAL_DIRECTIVES, ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';

//-------------------------------------------------------------------
// ITEMS-LIST
//-------------------------------------------------------------------
@Component({
    selector: 'items-list',
    template: require('./items.detail.html'),
    directives: [AgGridNg2, MODAL_DIRECTIVES]
})
class ItemList {
    @Input() items:Item[];
    @Output() selected = new EventEmitter();
    @Output() deleted = new EventEmitter();
    private gridOptions:GridOptions;
    private showGrid:boolean;
    private columnDefs:any[];
    private rowCount:string;
    private dataSource:{};
    @ViewChild('modal')
    modal: ModalComponent;
    pageSize = 10;

    constructor() {
        this.gridOptions = <GridOptions>{
            overlayLoadingTemplate: '<span class="ag-overlay-loading-center">Please wait while data are loading</span>'
        };
        this.createDataSource();
        this.createColumnDefs();
        this.showGrid = true;
    }

    private createDataSource() {
        // console.log('this.rowData ' + this.items.length);
        var $this = this;
        this.dataSource = {
            pageSize: $this.pageSize,
            getRows: function (params) {
                setTimeout(function () {

                    if (!$this.items) {
                        //in case user selected 'onPageSizeChanged()' before the json was loaded
                        return;
                    }
                    console.log('asking for ' + params.startRow + ' to ' + params.endRow);
                    var rowsThisPage = $this.items.slice(params.startRow, params.endRow);
                    var lastRow = -1;
                    if ($this.items.length <= params.endRow) {
                        lastRow = $this.items.length;
                    }
                    $this.gridOptions.api.sizeColumnsToFit();
                    $this.gridOptions.columnApi.getDisplayedCenterColumns();
                    params.successCallback(rowsThisPage, lastRow);
                }, 1000);
            }
        };
    }

    private createColumnDefs() {
        this.columnDefs = [
            {
                headerName: '#', checkboxSelection: true, suppressSorting: true,
                suppressMenu: true, pinned: true
            },
            {
                headerName: 'Name', field: 'name',
                pinned: true, suppressSorting: true,
                suppressMenu: true
            },
            {
                headerName: 'Description', field: 'description', pinned: true,
                suppressSorting: true, suppressMenu: true
            },
            { headerName: '',
                suppressMenu: true,
                suppressSorting: true,
                template:
                    `<button type="button" data-action-type="edit" class="btn btn-default">
               Edit
             </button>`
            }

        ];
    }

    public onRowClicked(e) {
        console.log('onRowClicked: ' + e.node.data.name);
        if (e.event.target !== undefined) {
            let data = e.data;
            let actionType = e.event.target.getAttribute('data-action-type');

            switch(actionType) {
                case 'edit':
                    return this.onActionEditClick(data);
            }
        }
    }

    public onActionEditClick(data: any){
        console.log('Edit action clicked', data);
        this.selected.emit(data);
    }

    private onPageSizeChanged(newPageSize) {
        this.pageSize = newPageSize;
        this.createDataSource();
    }


    private calculateRowCount() {
        if (this.gridOptions.api && this.items) {
            console.log('calculateRowCount()');
            var model = this.gridOptions.api.getModel();
            console.log(model);
            var totalRows = this.items.length;
            console.log(totalRows);
            var processedRows = model.getRowCount();
            console.log(processedRows);
            this.rowCount = processedRows.toLocaleString() + ' / ' + totalRows.toLocaleString();
            console.log(this.rowCount);
        }
    }

    private onModelUpdated() {
        console.log('onModelUpdated');
        this.calculateRowCount();
    }

    private onOpen() {
        this.modal.open();
    }

    private deleteItems() {
        var $this = this;
        console.log('onModelDeleted');
        var selectedRows = $this.gridOptions.api.getSelectedRows();
        selectedRows.forEach( function(selectedRow, index) {
            $this.deleted.emit(selectedRow);
        });
        $this.modal.close();
    }

    private onReady() {
        console.log('onReady');
        this.calculateRowCount();
    }

    public onRefreshAll() {
        // this.gridOptions.api.setRowData(this.items);
        // this.gridOptions.api.refreshView();
        this.createDataSource();
    }

    private onCellClicked($event) {
        console.log('onCellClicked: ' + $event.rowIndex + ' ' + $event.colDef.field);
    }

    private onCellValueChanged($event) {
        console.log('onCellValueChanged: ' + $event.oldValue + ' to ' + $event.newValue);
    }

    private onCellDoubleClicked($event) {
        console.log('onCellDoubleClicked: ' + $event.rowIndex + ' ' + $event.colDef.field);
    }

    private onCellContextMenu($event) {
        console.log('onCellContextMenu: ' + $event.rowIndex + ' ' + $event.colDef.field);
    }

    private onCellFocused($event) {
        console.log('onCellFocused: (' + $event.rowIndex + ',' + $event.colIndex + ')');
    }

    private onRowSelected($event) {
        // taking out, as when we 'select all', it prints to much to the console!!
        // console.log('onRowSelected: ' + $event.node.data.name);
    }

    private onSelectionChanged() {
        console.log('selectionChanged');
    }

    private onBeforeFilterChanged() {
        console.log('beforeFilterChanged');
    }

    private onAfterFilterChanged() {
        console.log('afterFilterChanged');
    }

    private onFilterModified() {
        console.log('onFilterModified');
    }

    private onBeforeSortChanged() {
        console.log('onBeforeSortChanged');
    }

    private onAfterSortChanged() {
        console.log('onAfterSortChanged');
    }

    private onVirtualRowRemoved($event) {
        // because this event gets fired LOTS of times, we don't print it to the
        // console. if you want to see it, just uncomment out this line
        // console.log('onVirtualRowRemoved: ' + $event.rowIndex);
    }

    private onQuickFilterChanged($event) {
        this.gridOptions.api.setQuickFilter($event.target.value);
    }

    // here we use one generic event to handle all the column type events.
    // the method just prints the event name
    private onColumnEvent($event) {
        console.log('onColumnEvent: ' + $event);
    }
}

//-------------------------------------------------------------------
// ITEM DETAIL
//-------------------------------------------------------------------
@Component({
    selector: 'item-detail',
    template: `
  <div class="item-card mdl-card mdl-shadow--2dp">
    <div class="mdl-card__title">
      <h2 class="mdl-card__title-text" *ngIf="selectedItem.id">Editing {{originalName}}</h2>
      <h2 class="mdl-card__title-text" *ngIf="!selectedItem.id">Create New Item</h2>
    </div>
    <div class="mdl-card__supporting-text">
      <form novalidate>
          <div class="mdl-textfield mdl-js-textfield">
            <label>Item Name</label>
            <input [(ngModel)]="selectedItem.name"
              placeholder="Enter a name"
              class="mdl-textfield__input" type="text">
          </div>

          <div class="mdl-textfield mdl-js-textfield">
            <label>Item Description</label>
            <input [(ngModel)]="selectedItem.description"
              placeholder="Enter a description"
              class="mdl-textfield__input" type="text">
          </div>
      </form>
    </div>
    <div class="mdl-card__actions">
        <button type="submit" (click)="cancelled.emit(selectedItem)"
          class="mdl-button mdl-js-button mdl-js-ripple-effect">Cancel</button>
        <button type="submit" (click)="saved.emit(selectedItem)"
          class="mdl-button mdl-js-button mdl-button--colored mdl-js-ripple-effect">Save</button>
    </div>
  </div>
  `
})
class ItemDetail {
    @Input('item') _item:Item;
    originalName:string;
    selectedItem:Item;
    @Output() saved = new EventEmitter();
    @Output() cancelled = new EventEmitter();

    set _item(value:Item) {
        if (value) this.originalName = value.name;
        this.selectedItem = Object.assign({}, value);
    }
}

//-------------------------------------------------------------------
// MAIN COMPONENT
//-------------------------------------------------------------------
@Component({
    selector: 'my-app',
    providers: [],
    template: `
  <div class="mdl-cell mdl-cell--6-col">
    <items-list [items]="items | async"
      (selected)="selectItem($event)" (deleted)="deleteItem($event)">
    </items-list>
  </div>
  <div class="mdl-cell mdl-cell--6-col">
    <item-detail
      (saved)="saveItem($event)" (cancelled)="resetItem($event)"
      [item]="selectedItem | async">Select an Item</item-detail>
  </div>
  `,
    directives: [ItemList, ItemDetail],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class App {
    items:Observable<Array<Item>>;
    selectedItem:Observable<Item>;
    @ViewChild(ItemList) vc:ItemList;

    constructor(private itemsService:ItemsService, private store:Store<AppStore>) {

    }

    ngOnInit() {
        this.items = this.itemsService.items;
        this.selectedItem = this.store.select('selectedItem');
        this.selectedItem.subscribe(v => console.log(v));

        this.itemsService.loadItems();
    }

    resetItem() {
        let emptyItem:Item = {id: null, name: '', description: ''};
        this.store.dispatch({type: 'SELECT_ITEM', payload: emptyItem});
        this.vc.onRefreshAll();
    }

    selectItem(item:Item) {
        this.store.dispatch({type: 'SELECT_ITEM', payload: item});
    }

    saveItem(item:Item) {
        this.itemsService.saveItem(item);
        this.resetItem();
    }

    deleteItem(item:Item) {
        this.itemsService.deleteItem(item);
        this.resetItem();
    }
}
